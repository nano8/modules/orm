---
layout: home

hero:
    name:    nano/modules/orm
    tagline: orm module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules
    -   title:   orm
        details: |
                 nano/modules/orm provides a simple way to use database in your nano application
---
