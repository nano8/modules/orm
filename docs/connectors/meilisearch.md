---
layout: doc
---

<script setup>
const args = {
    defineMeiliSearch: [
        {
            name: 'host',
            type: 'string',
        },
        {
            name: 'port',
            type: 'int',
        },
        {
            name: 'password',
            type: 'string',
        },
    ],
    useMeiliSearch: [],
};
</script>

## usage

`defineMeiliSearch` is a function that defines a meilisearch connection

```php
useNano()->withModule(new OrmModule());

defineMeiliSearch('localhost', 7700, 'password');

useNano()->start();
```

## <Types fn="defineMeiliSearch" r="void" :args="args.defineMeiliSearch" /> {#defineMeiliSearch}

```php
defineMeiliSearch('localhost', 7700, 'password');
```

## <Types fn="useMeiliSearch" r="MeiliSearch" :args="args.useMeiliSearch" /> {#useMeiliSearch}

you shouldn't use this function directly, it's used internally by the model
but if you want to use it, you can use it like this:

```php
useMeiliSearch()->insert('table_name', 1, ['name' => 'John Doe']);
```

