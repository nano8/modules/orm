---
layout: doc
---

<script setup>
const args = {
    defineRedis: [
        {
            name: 'host',
            type: 'string',
        },
        {
            name: 'port',
            type: 'int',
        },
        {
            name: 'password',
            type: 'string',
        },
        {
            name: 'expire',
            type: 'int',
        },
    ],
    useRedis: [],
};
</script>

## usage

`defineRedis` is a function that defines a redis connection

```php
useNano()->withModule(new OrmModule());

defineRedis('localhost', 6379, 'password', 3600);

useNano()->start();
```

## <Types fn="defineRedis" r="void" :args="args.defineRedis" /> {#defineRedis}

```php
defineRedis('localhost', 6379, 'password', 3600);
```

## <Types fn="useRedis" r="Redis" :args="args.useRedis" /> {#useRedis}

you shouldn't use this function directly, it's used internally by the model
but if you want to use it, you can use it like this:

```php
useRedis()->insert('table_name', 1, ['name' => 'John Doe']);
```

