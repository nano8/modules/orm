---
layout: doc
---

<script setup>
const args = {
    defineDB: [
        {
            name: 'host',
            type: 'string',
        },
        {
            name: 'port',
            type: 'int',
        },
        {
            name: 'database',
            type: 'string',
        },
        {
            name: 'user',
            type: 'string',
        },
        {
            name: 'password',
            type: 'string',
        },
        {
            name: 'driver',
            type: 'string',
        },
    ],
    useDB: [],
};
</script>

## usage

`defineDB` is a function that defines a database connection

```php
useNano()->withModule(new OrmModule());

defineDB('localhost', 3306, 'db_name', 'username', 'password');

useNano()->start();
```

## <Types fn="defineDB" r="void" :args="args.defineDB" /> {#defineDB}

by default, the driver is `mysql`
```php
defineDB('localhost', 3306, 'db_name', 'username', 'password');
```

to use another driver, pass it as the last argument
```php
defineDB('localhost', 5432, 'db_name', 'username', 'password', 'pgsql');
```

## <Types fn="useDB" r="Connection" :args="args.useDB" /> {#useDB}

you shouldn't use this function directly, it's used internally by the model
but if you want to use it, you can use it like this:

```php
useDB()->table('table_name')->select('column_name')->get();
```

