---
layout: doc
---

<script setup>
const args = {
    tryFromCase: [
        {
            name: 'case',
            type: 'string',
        },
    ],
};
</script>

## usage

`ComparisonType` is a enum that contains the available comparison types

```php
use laylatichy\nano\modules\orm\model\enums\ComparisonType;

ComparisonType::GREATER_THAN
```
