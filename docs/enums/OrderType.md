---
layout: doc
---

<script setup>
const args = {
    repository: [],
};
</script>

## usage

`OrderType` is a enum that contains the available order types

```php
use laylatichy\nano\modules\orm\model\enums\OrderType;

OrderType::ASC
OrderType::DESC
```
