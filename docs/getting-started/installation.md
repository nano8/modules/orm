---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-orm
```

## registering module

```php
use laylatichy\nano\modules\orm\OrmModule;

useNano()->withModule(new OrmModule());

// define your connectors here
```

