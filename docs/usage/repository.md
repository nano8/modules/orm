---
layout: doc
---

<script setup>
const args = {
    useRepository: [
        {
            name: 'class',
            type: 'string',
        },
        {
            name: 'request',
            type: 'Request',
        },
    ],
};
</script>

## usage

`useRepository` is a helper to interact with your database and grab paginated data from it,

data is returned as an array of objects, each object represents a row in the database

```php
// return shape
[
    data: [],
    prev: bool,
    next: bool,
]
```

## <Types fn="useRepository" r="array" :args="args.useRepository" /> {#useRepository}

```php
useRouter()->get('/users', fn (Request $request) => useResponse()
    ->withJson([
        'users' => useRepository(User::class, $request),
    ])
```

## limit

to limit the number of rows returned from the database, you can use the `limit` while making requests

```ts
const params = {
    limit: 10,
};

axios.get('/users', { params }); // returns 10 rows
```

## before

to filter the rows returned from the database, you can use the `before` while making requests as a get parameter

```ts
const params = {
    before: 100,
};

axios.get('/users', { params }); // returns rows with id less than 100
```

## after

to filter the rows returned from the database, you can use the `after` while making requests

```ts
const params = {
    after: 100,
};

axios.get('/users', { params }); // returns rows with id greater than 100
```

## filters

to filter the rows returned from the database, you can use the `filters` while making requests

```ts
const params = {
    filters: {
        age: {
            value: 18,
            type:  'GREATER_THAN', // or use operator '>'
        },
    },
};

axios.get('/users', { params }); // returns rows with a column age greater than 18
```
