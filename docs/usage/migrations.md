---
layout: doc
---

<script setup>
const args = {
    string: [
        {
            name: 'name',
            type: 'string',
        },
        {
            name: 'nullable',
            type: 'bool',
        },
        {
            name: 'default',
            type: '?string',
        },
        {
            name: 'after',
            type: '?string',
        },
    ],
    number: [
        {
            name: 'name',
            type: 'string',
        },
        {
            name: 'nullable',
            type: 'bool',
        },
        {
            name: 'default',
            type: '?int',
        },
        {
            name: 'after',
            type: '?string',
        },
        {
            name: 'limit',
            type: '?int',
        },
        {
            name: 'signed',
            type: 'bool',
        },
    ],
    big_int: [
        {
            name: 'name',
            type: 'string',
        },
        {
            name: 'nullable',
            type: 'bool',
        },
        {
            name: 'default',
            type: '?int',
        },
        {
            name: 'after',
            type: '?string',
        },
        {
            name: 'limit',
            type: '?int',
        },
        {
            name: 'signed',
            type: 'bool',
        },
    ],
    bool: [
        {
            name: 'name',
            type: 'string',
        },
        {
            name: 'default',
            type: '?int',
        },
        {
            name: 'nullable',
            type: 'bool',
        },
        {
            name: 'after',
            type: '?string',
        },
    ],
    text: [
        {
            name: 'name',
            type: 'string',
        },
        {
            name: 'nullable',
            type: 'bool',
        },
        {
            name: 'default',
            type: '?int',
        },
        {
            name: 'limit',
            type: '?int',
        },
        {
            name: 'after',
            type: '?string',
        },
    ],
    date: [
        {
            name: 'name',
            type: 'string',
        },
        {
            name: 'nullable',
            type: 'bool',
        },
        {
            name: 'after',
            type: '?string',
        },
    ],
    datetime: [
        {
            name: 'name',
            type: 'string',
        },
        {
            name: 'nullable',
            type: 'bool',
        },
        {
            name: 'after',
            type: '?string',
        },
    ],
    key: [
        {
            name: 'name',
            type: 'string',
        },
        {
            name: 'nullable',
            type: 'bool',
        },
        {
            name: 'after',
            type: '?string',
        },
    ],
    index: [
        {
            name: 'columns',
            type: 'array',
        },
        {
            name: 'unique',
            type: 'bool',
        },
        {
            name: 'name',
            type: '?string',
        },
    ],
};
</script>

## usage

orm module uses [phinx](https://phinx.org/) under the hood for migrations 

```php
// example phinx.php
return [
    'paths'         => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds'      => '%%PHINX_CONFIG_DIR%%/db/seeds',
    ],
    'environments'  => [
        'default_migration_table' => 'phinxlog',
        'default_environment'     => 'dev',
        'dev'                     => [
            'adapter' => 'mysql',
            'host'    => '127.0.0.1',
            'name'    => 'local-db',
            'user'    => 'root',
            'pass'    => 'root',
            'port'    => '3306',
            'charset' => 'utf8mb4',
        ],
    ],
    'version_order' => 'creation',
];

```

example migration file

```php
// 20240104072338_users_table.php
use laylatichy\nano\modules\orm\NanoMigration;

final class UsersTable extends NanoMigration {
    public function change(): void {
        parent::create('users');
    }

    public function columns(): void {
        $this
            ->string('email')
            ->string('password', true)
            ->string('name');
            
        // columns
        // id, created_at, updated_at
        // are created automatically
    }
}
```

and you can run your migrations with the following command

```sh
vendor/bin/phinx migrate
```

## <Types fn="string" r="self" :args="args.string" /> {#string}

create varchar column

```php
$this->string('email');

$this->string('email', true); // nullable

$this->string('email', default: 'default value'); // default value

$this->string('email', after: 'name'); // after name
```

## <Types fn="number" r="self" :args="args.number" /> {#number}

create int column

```php
$this->number('age');

$this->number('age', true); // nullable

$this->number('age', default: 11); // default value

$this->number('age', after: 'name'); // after name

$this->number('age', limit: 65535); // limit 65535

$this->number('age', signed: true); // signed int
```

## <Types fn="big_int" r="self" :args="args.big_int" /> {#big_int}

create biginteger column

```php
$this->big_int('age');

$this->big_int('age', true); // nullable

$this->big_int('age', default: 11); // default value

$this->big_int('age', after: 'name'); // after name

$this->big_int('age', limit: 65535); // limit 65535

$this->big_int('age', signed: true); // signed int
```

## <Types fn="bool" r="self" :args="args.bool" /> {#bool}

create boolean column

```php
$this->bool('active');

$this->bool('active', nullable: true); // nullable

$this->bool('active', default: true); // default value

$this->bool('active', after: 'name'); // after name
```

## <Types fn="text" r="self" :args="args.text" /> {#text}

create text column

```php
$this->text('description');

$this->text('description', true); // nullable

$this->text('description', default: 'default value'); // default value

$this->text('description', limit: 65535); // limit 65535

$this->text('description', after: 'name'); // after name
```

## <Types fn="date" r="self" :args="args.date" /> {#date}

create date column

```php
$this->date('birthday');

$this->date('birthday', nullable: true); // nullable

$this->date('birthday', after: 'name'); // after name
```

## <Types fn="datetime" r="self" :args="args.datetime" /> {#datetime}

create datetime column

```php
$this->datetime('created_at');

$this->datetime('created_at', nullable: true); // nullable

$this->datetime('created_at', after: 'name'); // after name
```

## <Types fn="key" r="self" :args="args.key" /> {#key}

create relation key column

```php
$this->key('address_id'); // user.address_id -> address.id

$this->key('address_id', nullable: true); // nullable

$this->key('address_id', after: 'name'); // after name
```

## <Types fn="index" r="self" :args="args.index" /> {#index}

create index

```php
$this->index(['email', 'username']);

$this->index(['email', 'username'], unique: false); // non unique index

$this->index(['email', 'username'], name: 'email_username'); // custom index name
```

