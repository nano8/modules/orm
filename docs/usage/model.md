---
layout: doc
---

<script setup>
const args = {
    model: [],
    insert: [],
    update: [],
    delete: [],
    fetch: [],
    byId: [
        {
            name: 'id',
            type: 'int',
        },
    ],
    by: [
        {
            name: 'column',
            type: 'string',
        },
        {
            name: 'value',
            type: 'int|string|null',
        },
    ],
    limit: [
        {
            name: 'limit',
            type: 'int',
        },
    ],
    offset: [
        {
            name: 'offset',
            type: 'int',
        },
    ],
    where: [
        {
            name: 'column',
            type: 'string',
        },
        {
            name: 'value',
            type: 'int|string|null',
        },
        {
            name: 'comparison',
            type: 'ComparisonType',
        },
    ],
    order: [
        {
            name: 'by',
            type: 'string',
        },
        {
            name: 'type',
            type: 'OrderType',
        },
    ],
    single: [],
    all: [],
    search: [
        {
            name: 'term',
            type: 'string',
        },
    ],
    refresh: [],
    count: [],
    belongsTo: [
        {
            name: 'class',
            type: 'string',
        },
    ],
    hasOne: [
        {
            name: 'class',
            type: 'string',
        },
    ],
    maybeHasOne: [
        {
            name: 'class',
            type: 'string',
        },
    ],
    hasMany: [
        {
            name: 'class',
            type: 'string',
        },
    ],
};
</script>

## usage

`Model` is a class to help you create a model for your database

```php
use laylatichy\nano\modules\orm\Model;

class User extends Model {
    public function jsonSerialize(): array {
        return [];
    }
}
```

and that's it, you have a model that can be used with the database

`jsonSerialize` is a method that is required by the `JsonSerializable` interface,
it is used to serialize the model to json for nano responses

## enums

you can use enums in your models they will be cast to a value when inserted or tryFrom will be used to cast the value for fetching

```php
enum UserStatus: int {
    case ACTIVE = 1;
    case INACTIVE = 0;
}

class User extends Model {
    public UserStatus $status;
}

$user = new User();
$user->status = UserStatus::ACTIVE;
$user->insert();

// record in the database will be {"status": 1}

$user = User::fetch()->byId(1);
echo $user->status; // UserStatus::ACTIVE
```

## redis

to enable redis caching for this model simply set the `redis` property to `true`

```php
class User extends Model {
    protected bool $redis = true;
}
```

## meilisearch

to enable meilisearch indexing for this model simply set the `meilisearch` property to `true`

```php
class User extends Model {
    protected bool $meilisearch = true;
}
```

## <Types fn="insert" r="int" :args="args.insert" /> {#insert}

`insert` is a method that inserts a new record into the database

```php
class User extends Model {
    public string $name;
}

$user = new User();
$user->name = 'John Doe';
$user->insert(); // returns the id of the inserted record
```

if you have a property that is nullable you can ignore it, and it will be cast to null

```php
class User extends Model {
    public ?string $name;
}

$user = new User();
$user->insert();

echo $user->name; // null
```

inserting child models is also supported

```php
class Address extends Model {
    public string $city;
}

class User extends Model {
    public Address $address;
}

$address = new Address();
$address->city = 'New York';

$user = new User();
$user->address = $address;
$user->insert(); 
// notice that we don't need to insert the address first,
// it will be inserted automatically
// note that the address will be inserted first, then the user
// and user table should have a column address_id
```

## <Types fn="update" r="void" :args="args.update" /> {#update}

`update` is a method that updates a record in the database

```php
class User extends Model {
    public string $name;
}

$user = new User();
$user->name = 'John Doe';
$user->insert();

$user->name = 'Jane Doe Updated';
$user->update();
```

## <Types fn="delete" r="void" :args="args.delete" /> {#delete}

`delete` is a method that deletes a record from the database

```php
class User extends Model {
    public string $name;
}

$user = new User();
$user->name = 'John Doe';
$user->insert();

$user->delete();
```

## <Types fn="fetch" r="static" :args="args.fetch" /> {#fetch}

`fetch` is a method that starts a query builder

```php
class User extends Model {
    public string $name;
}

User::fetch();
```

## <Types fn="byId" r="?static" :args="args.byId" /> {#byId}

`byId` is a method that fetches a record by id

```php
class User extends Model {
    public string $name;
}

$user = User::fetch()->byId(1);
```

you can fetch child at the same time

```php
class Address extends Model {
    public string $city;
}

class User extends Model {
    public Address $address;
}

$user = User::fetch()->byId(1);

echo $user->address->city; // New York
```

## <Types fn="by" r="static" :args="args.by" /> {#by}

`by` is a method that fetches a record by a column

```php
class User extends Model {
    public string $name;
}

$user = User::fetch()->by('name', 'John Doe')->single();
```

## <Types fn="limit" r="static" :args="args.limit" /> {#limit}

`limit` is a method that limits the number of records to fetch

```php
class User extends Model {
    public string $name;
}

$users = User::fetch()->limit(10)->all();
```

## <Types fn="offset" r="static" :args="args.offset" /> {#offset}

`offset` is a method that sets the offset of the records to fetch

```php
class User extends Model {
    public string $name;
}

$users = User::fetch()->offset(10)->all();
```

## <Types fn="where" r="static" :args="args.where" /> {#where}

`where` is a method that sets a where clause for the query

```php
class User extends Model {
    public string $name;
}

$users = User::fetch()->where('name', 'John Doe', ComparisonType::EQUAL)->all();
```

## <Types fn="order" r="static" :args="args.order" /> {#order}

`order` is a method that sets the order of the records to fetch

```php
class User extends Model {
    public string $name;
}

$users = User::fetch()->order('name', OrderType::DESC)->all();
```

## <Types fn="single" r="?static" :args="args.single" /> {#single}

`single` is a method that fetches a single record

```php
class User extends Model {
    public string $name;
}

$user = User::fetch()->single();
```

## <Types fn="all" r="static[]" :args="args.all" /> {#all}

`all` is a method that fetches all records

```php
class User extends Model {
    public string $name;
}

$users = User::fetch()->all();
```

## <Types fn="search" r="static[]" :args="args.search" /> {#search}

`search` is a method that searches for records

note that this method is only available if you have meilisearch enabled

```php
class User extends Model {
    public string $name;
}

$users = User::fetch()->search('John');
```

## <Types fn="refresh" r="static" :args="args.refresh" /> {#refresh}

`refresh` is a method that refreshes the model

```php
class User extends Model {
    public string $name;
}

$user = User::fetch()->byId(1);
$user->name = 'John Doe Updated';

$user->refresh();
echo $user->name; // John Doe
```

## <Types fn="count" r="int" :args="args.count" /> {#count}

```php
class User extends Model {
    public string $name;
}

$count = User::fetch()->count(); // 0

$user = new User();
$user->name = 'John Doe';
$user->insert();

$count = User::fetch()->count(); // 1
```

## <Types fn="belongsTo" r="Model" :args="args.belongsTo" /> {#belongsTo}

`belongsTo` is a method that fetches a record by id from another model

```php
class User extends Model {
    public string $name;
}

class Address extends Model {
    public int $user_id;

    public function user(): User {
        return $this->belongsTo(User::class);
    }
}
```

## <Types fn="hasOne" r="Model" :args="args.hasOne" /> {#hasOne}

`hasOne` is a method that fetches a record by id from another model

```php
class User extends Model {
    public string $name;
    public int $address_id;
    
    public function address(): Address {
        return $this->hasOne(Address::class);
    }
}

class Address extends Model {
    public int $user_id;
}
```

## <Types fn="maybeHasOne" r="?Model" :args="args.maybeHasOne" /> {#maybeHasOne}

`maybeHasOne` is a method that fetches a record by id from another model or returns null if not found

```php
class User extends Model {
    public string $name;
    public int $address_id;
    
    public function address(): ?Address {
        return $this->maybeHasOne(Address::class);
    }
}

class Address extends Model {
    public int $user_id;
}
```

## <Types fn="hasMany" r="Model[]" :args="args.hasMany" /> {#hasMany}

`hasMany` is a method that fetches child records by id from another model

```php
class User extends Model {
    public string $name;
    
    /**
     * @return Address[]
     */
    public function addresses(): array {
        return $this->hasMany(Address::class);
    }
}

class Address extends Model {
    public int $user_id;
}
```

