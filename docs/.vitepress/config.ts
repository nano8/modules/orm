import { defineConfig } from 'vitepress';

export default defineConfig({
    lastUpdated:   true,
    sitemap:       {
        hostname: 'https://nano8.gitlab.io/modules/orm/',
    },
    title:         'nano/modules/orm',
    titleTemplate: 'nano/modules/orm - documentation',
    description:   'simple orm for laylatichy/nano framework',
    base:          '/modules/orm/',
    themeConfig:   {
        lastUpdated: {
            text:          'updated',
            formatOptions: {
                dateStyle: 'long',
                timeStyle: 'medium',
            },
        },
        search:      {
            provider: 'local',
        },
        nav:         [],
        sidebar:     [
            {
                text:  'getting started',
                items: [
                    {
                        text: 'introduction',
                        link: '/getting-started/introduction',
                    },
                    {
                        text: 'installation',
                        link: '/getting-started/installation',
                    },
                ],
            },
            {
                text:  'connectors',
                items: [
                    {
                        text: 'db',
                        link: '/connectors/db',
                    },
                    {
                        text: 'redis',
                        link: '/connectors/redis',
                    },
                    {
                        text: 'meilisearch',
                        link: '/connectors/meilisearch',
                    },
                ],
            },
            {
                text:  'enums',
                items: [
                    {
                        text: 'OrderType',
                        link: '/enums/OrderType',
                    },
                    {
                        text: 'ComparisonType',
                        link: '/enums/ComparisonType',
                    },
                ],
            },
            {
                text:  'usage',
                items: [
                    {
                        text: 'model',
                        link: '/usage/model',
                    },
                    {
                        text: 'repository',
                        link: '/usage/repository',
                    },
                    {
                        text: 'migrations',
                        link: '/usage/migrations',
                    },
                ],
            },
            {
                text: 'nano documentation',
                link: 'https://nano.laylatichy.com',
            },
        ],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/nano8/modules/orm',
            },
        ],
        editLink:    {
            text:    'edit this page',
            pattern: 'https://gitlab.com/nano8/modules/orm/-/edit/dev/docs/:path',
        },
    },
});
