<?php

namespace laylatichy\nano\modules\orm\db;

use Exception;
use Illuminate\Database\Capsule\Manager;
use laylatichy\nano\modules\orm\Connection;

class DBConnection extends Connection {
    public Manager $client;

    public function __construct(
        private readonly string $host,
        private readonly int $port,
        private readonly string $database,
        private readonly string $user,
        private readonly string $password,
        private readonly string $driver, // mysql for mysql, pgsql for postgresql
        private readonly string $charset,
        private readonly string $collation,
        private readonly array $options,
    ) {
        $this->connect();
    }

    public function connect(): void {
        $this->client = new Manager();

        $this->client->addConnection([
            'driver'    => $this->driver,
            'host'      => $this->host,
            'port'      => $this->port,
            'database'  => $this->database,
            'username'  => $this->user,
            'password'  => $this->password,
            'charset'   => $this->charset,
            'collation' => $this->collation,
            'options'   => $this->options,
        ]);

        try {
            $this->client->getConnection()->getPdo();
        } catch (Exception $e) {
            useNanoException("db connection failed: {$e->getMessage()}");
        }
    }

    public function disconnect(): void {
        unset($this->client);
    }
}
