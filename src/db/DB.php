<?php

namespace laylatichy\nano\modules\orm\db;

class DB {
    public DBConnection $connection;

    public function setConnection(DBConnection $connection): void {
        if (isset($this->connection)) {
            useNanoException('db connection already set');
        }

        $this->connection = $connection;
    }
}
