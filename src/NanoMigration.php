<?php

namespace laylatichy\nano\modules\orm;

use Phinx\Db\Table;
use Phinx\Migration\AbstractMigration;

abstract class NanoMigration extends AbstractMigration {
    public Table $table;

    public Table $journal;

    public bool $hasJournal = false;

    abstract public function change(): void;

    public function create(string $table, ?string $journalTable = null): void {
        $this->table = $this->table($table, [
            'id'          => false,
            'primary_key' => 'id',
        ])->addColumn('id', 'biginteger', [
            'identity' => true,
            'signed'   => false,
        ]);

        if ($journalTable !== null) {
            $this->hasJournal = true;

            $this->journal = $this->table($journalTable, [
                'id' => false,
            ])->addColumn('id', 'biginteger', [
                'identity' => false,
                'signed'   => false,
                'null'     => false,
            ]);
        }

        $this->columns();

        $this->timestamps();

        $this->triggers();

        $this->data();
    }

    public function update(string $table, ?string $journalTable = null): void {
        $this->table = $this->table($table);

        if ($journalTable !== null) {
            $this->hasJournal = true;

            $this->journal = $this->table($journalTable);
        }

        $this->columns();

        $this->table->update();

        if ($this->hasJournal) {
            $this->journal->update();
        }

        $this->triggers();

        $this->data();
    }

    public function timestamps(): void {
        $this->table->addColumn('created_at', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
            'null'    => false,
        ])->addColumn('updated_at', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
            'null'    => false,
        ])->create();

        if ($this->hasJournal) {
            $this->journal->addColumn('created_at', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
                'null'    => false,
            ])->addColumn('updated_at', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
                'null'    => false,
            ])->addColumn('audit_type', 'enum', [
                'null'   => false,
                'values' => ['create', 'update', 'delete'],
            ])->addColumn('audit_time', 'datetime', [
                'default' => 'CURRENT_TIMESTAMP',
                'null'    => false,
            ])->create();
        }
    }

    public function insert(array $data): void {
        $this->table->insert($data)->saveData();
    }

    public function string(
        string $name,
        bool $nullable = false,
        ?string $default = null,
        ?string $after = null
    ): self {
        $options = [
            'null' => $nullable,
        ];

        if ($default !== null) {
            $options['default'] = $default;
        }

        if ($after !== null) {
            $options['after'] = $after;
        }

        $this->table->addColumn($name, 'string', $options);

        if ($this->hasJournal) {
            $this->journal->addColumn($name, 'string', $options);
        }

        return $this;
    }

    public function number(
        string $name,
        bool $nullable = false,
        ?int $default = null,
        ?string $after = null,
        ?int $limit = null,
        bool $signed = false,
    ): self {
        $options = [
            'null' => $nullable,
        ];

        if ($default !== null) {
            $options['default'] = $default;
        }

        if ($after !== null) {
            $options['after'] = $after;
        }

        if ($limit !== null) {
            $options['limit'] = $limit;
        }

        $options['signed'] = $signed;

        $this->table->addColumn($name, 'integer', $options);

        if ($this->hasJournal) {
            $this->journal->addColumn($name, 'integer', $options);
        }

        return $this;
    }

    public function big_int(
        string $name,
        bool $nullable = false,
        ?int $default = null,
        ?string $after = null,
        ?int $limit = null,
        bool $signed = false,
    ): self {
        $options = [
            'null' => $nullable,
        ];

        if ($default !== null) {
            $options['default'] = $default;
        }

        if ($after !== null) {
            $options['after'] = $after;
        }

        if ($limit !== null) {
            $options['limit'] = $limit;
        }

        $options['signed'] = $signed;

        $this->table->addColumn($name, 'biginteger', $options);

        if ($this->hasJournal) {
            $this->journal->addColumn($name, 'biginteger', $options);
        }

        return $this;
    }

    public function bool(
        string $name,
        bool $default = false,
        bool $nullable = false,
        ?string $after = null
    ): self {
        $options = [
            'default' => $default,
            'null'    => $nullable,
        ];

        if ($after !== null) {
            $options['after'] = $after;
        }

        $this->table->addColumn($name, 'boolean', $options);

        if ($this->hasJournal) {
            $this->journal->addColumn($name, 'boolean', $options);
        }

        return $this;
    }

    public function text(
        string $name,
        bool $nullable = false,
        ?string $default = null,
        int $limit = 65535,
        ?string $after = null,
    ): self {
        $options = [
            'limit' => $limit,
            'null'  => $nullable,
        ];

        if ($default !== null) {
            $options['default'] = $default;
        }

        if ($after !== null) {
            $options['after'] = $after;
        }

        $this->table->addColumn($name, 'text', $options);

        if ($this->hasJournal) {
            $this->journal->addColumn($name, 'text', $options);
        }

        return $this;
    }

    public function date(string $name, bool $nullable = false, ?string $after = null): self {
        $options = [
            'null' => $nullable,
        ];

        if ($after !== null) {
            $options['after'] = $after;
        }

        $this->table->addColumn($name, 'date', $options);

        if ($this->hasJournal) {
            $this->journal->addColumn($name, 'date', $options);
        }

        return $this;
    }

    public function datetime(string $name, bool $nullable = false, ?string $after = null): self {
        $options = [
            'null' => $nullable,
        ];

        if ($after !== null) {
            $options['after'] = $after;
        }

        $this->table->addColumn($name, 'datetime', $options);

        if ($this->hasJournal) {
            $this->journal->addColumn($name, 'datetime', $options);
        }

        return $this;
    }

    public function key(string $name, bool $nullable = false, ?string $after = null, ?string $column = null, ?string $on_delete = null): self {
        $options = [
            'signed' => false,
            'null'   => $nullable,
        ];

        if ($after !== null) {
            $options['after'] = $after;
        }

        $this->table->addColumn($column ?? $name, 'biginteger', $options)->addForeignKey(
            $column ?? $name,
            str_replace('_id', '', $name),
            'id',
            [
                'delete' => $on_delete ?: ($nullable ? 'SET_NULL' : 'CASCADE'),
            ]
        );

        if ($this->hasJournal) {
            $this->journal->addColumn($name, 'biginteger', $options);
        }

        return $this;
    }

    public function index(
        array $columns,
        bool $unique = true,
        ?string $name = null,
    ): self {
        $options = [
            'unique' => $unique,
        ];

        if ($name !== null) {
            $options['name'] = $name;
        } else {
            $cols            = implode('_', $columns);
            $cols            = str_replace('-', '_', $cols);
            $cols            = str_replace(' ', '_', $cols);
            $cols            = strtolower($cols);
            $options['name'] = "idx_{$cols}";
        }

        $this->table->addIndex($columns, $options);

        return $this;
    }

    public function columns(): void {}

    public function data(): void {}

    public function triggers(): void {}

    /**
     * @param string[] $columns
     */
    public function journal(array $columns): void {
        $this->execute("Drop TRIGGER IF EXISTS {$this->table->getName()}_insert");
        $this->execute("Drop TRIGGER IF EXISTS {$this->table->getName()}_update");
        $this->execute("Drop TRIGGER IF EXISTS {$this->table->getName()}_delete");

        $this->execute(
            sprintf(
                'CREATE TRIGGER %s_insert AFTER INSERT ON %s FOR EACH ROW
            INSERT INTO %s VALUES (%s, %s, %s, %s, %s, %s)',
                $this->table->getName(),
                $this->table->getName(),
                $this->journal->getName(),
                'NEW.id',
                implode(', ', array_map(fn ($column) => "NEW.{$column}", $columns)),
                'NEW.created_at',
                'NEW.updated_at',
                '"create"',
                'NOW()',
            )
        );

        $this->execute(
            sprintf(
                'CREATE TRIGGER %s_update AFTER UPDATE ON %s FOR EACH ROW
            INSERT INTO %s VALUES (%s, %s, %s, %s, %s, %s)',
                $this->table->getName(),
                $this->table->getName(),
                $this->journal->getName(),
                'NEW.id',
                implode(', ', array_map(fn ($column) => "NEW.{$column}", $columns)),
                'NEW.created_at',
                'NEW.updated_at',
                '"update"',
                'NOW()',
            )
        );

        $this->execute(
            sprintf(
                'CREATE TRIGGER %s_delete AFTER DELETE ON %s FOR EACH ROW
            INSERT INTO %s VALUES (%s, %s, %s, %s, %s, %s)',
                $this->table->getName(),
                $this->table->getName(),
                $this->journal->getName(),
                'OLD.id',
                implode(', ', array_map(fn ($column) => "OLD.{$column}", $columns)),
                'OLD.created_at',
                'OLD.updated_at',
                '"delete"',
                'NOW()',
            )
        );
    }
}