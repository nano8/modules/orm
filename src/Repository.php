<?php

namespace laylatichy\nano\modules\orm;

use JetBrains\PhpStorm\ArrayShape;
use laylatichy\nano\core\request\Request;
use laylatichy\nano\modules\orm\model\enums\ComparisonType;
use laylatichy\nano\modules\orm\model\enums\OrderType;
use laylatichy\nano\modules\orm\repository\Query;

class Repository {
    private bool $prev = false;

    private bool $next = false;

    /**
     * @var Model[]
     */
    private array $data = [];

    private readonly Query $query;

    public function __construct(
        private readonly Model $model,
        Request $request,
    ) {
        $this->query = new Query($request);
    }

    #[ArrayShape(['data' => 'Model[]', 'prev' => 'bool', 'next' => 'bool'])]
    public function fetch(): array {
        if ($this->query->limit !== 0) {
            $this->model->limit($this->query->limit + 1);
        }

        foreach ($this->query->filters as $filter) {
            $this->model->where(
                $filter->column,
                $filter->value,
                $filter->comparison,
            );
        }

        if ($this->query->after) {
            $this->model->where(
                $this->query->order_by,
                $this->query->after,
                $this->query->order_type === OrderType::ASC
                ? ComparisonType::GREATER_THAN
                : ComparisonType::LESS_THAN,
            );

            $this->model->order(
                $this->query->order_by,
                $this->query->order_type === OrderType::ASC
                    ? OrderType::ASC
                    : OrderType::DESC,
            );
        }

        if ($this->query->before) {
            $this->model->where(
                $this->query->order_by,
                $this->query->before,
                $this->query->order_type === OrderType::ASC
                            ? ComparisonType::LESS_THAN
                            : ComparisonType::GREATER_THAN,
            );

            $this->model->order(
                $this->query->order_by,
                $this->query->order_type === OrderType::ASC
                    ? OrderType::DESC
                    : OrderType::ASC,
            );
        }

        if (!$this->query->after && !$this->query->before) {
            $this->model->order(
                $this->query->order_by,
                $this->query->order_type === OrderType::ASC
                    ? OrderType::ASC
                    : OrderType::DESC,
            );
        }

        $this->data = $this->model->all();

        if ($this->query->before) {
            $this->data = array_reverse($this->data);
        }

        if ($this->query->limit !== 0) {
            if (count($this->data) > $this->query->limit) {
                if ($this->query->after) {
                    array_pop($this->data);
                    $this->next = true;
                } elseif ($this->query->before) {
                    array_shift($this->data);
                    $this->prev = true;
                } else {
                    $this->next = true;
                    array_pop($this->data);
                }
            }

            if ($this->query->after || $this->query->before) {
                $model = $this->model::fetch()->limit(1);

                foreach ($this->query->filters as $filter) {
                    $model->where(
                        $filter->column,
                        $filter->value,
                        $filter->comparison,
                    );
                }

                if ($this->query->after) {
                    $model->where(
                        $this->query->order_by,
                        $this->query->after,
                        $this->query->order_type === OrderType::ASC
                    ? ComparisonType::LESS_THAN_OR_EQUAL
                    : ComparisonType::GREATER_THAN_OR_EQUAL,
                    );

                    $model->order(
                        $this->query->order_by,
                        $this->query->order_type === OrderType::ASC
                            ? OrderType::ASC
                            : OrderType::DESC,
                    );
                }

                if ($this->query->before) {
                    $model->where(
                        $this->query->order_by,
                        $this->query->before,
                        $this->query->order_type === OrderType::ASC
                            ? ComparisonType::GREATER_THAN_OR_EQUAL
                            : ComparisonType::LESS_THAN_OR_EQUAL,
                    );

                    $model->order(
                        $this->query->order_by,
                        $this->query->order_type === OrderType::ASC
                            ? OrderType::DESC
                            : OrderType::ASC,
                    );
                }

                if ($model->single()) {
                    if ($this->query->after) {
                        $this->prev = true;
                    } elseif ($this->query->before) {
                        $this->next = true;
                    }
                }
            }
        }

        return [
            'data'  => $this->data,
            'prev'  => $this->prev,
            'next'  => $this->next,
        ];
    }
}