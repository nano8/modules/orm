<?php

namespace laylatichy\nano\modules\orm;

use BackedEnum;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use JsonSerializable;
use laylatichy\nano\core\exception\NanoException;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\modules\orm\db\DB;
use laylatichy\nano\modules\orm\meilisearch\MeiliSearch;
use laylatichy\nano\modules\orm\model\enums\ComparisonType;
use laylatichy\nano\modules\orm\model\enums\OrderType;
use laylatichy\nano\modules\orm\model\Meta;
use laylatichy\nano\modules\orm\model\Property;
use laylatichy\nano\modules\orm\redis\Redis;
use ReflectionClass;

abstract class Model implements JsonSerializable {
    protected int $id;

    protected string $created_at;

    protected string $updated_at;

    protected string $source;

    protected bool $redis       = false;

    protected bool $meiliSearch = false;

    private Builder $query;

    private Builder $query_replica;

    private readonly Meta $meta;

    private bool $deleted = false;

    public function __construct() {
        $this->meta = new Meta(static::class);
    }

    public function getId(): int {
        return $this->id;
    }

    public function insert(): int {
        $this->setCreatedAt();
        $this->setUpdatedAt();

        $data = [];

        foreach ($this->meta->properties as $property) {
            $data[$property->column] = $this->processPropertiesInsert($property);
        }

        $this->query = useDB()->table($this->meta->table);

        $this->id = $this->query->insertGetId($data);

        if ($this->redis) {
            useRedis()->insert($this->meta->table, $this->id, $data);
        }

        if ($this->meiliSearch) {
            useMeiliSearch()->insert($this->meta->table, $this->id, $data);
        }

        $this->setSource(DB::class);

        $this->disconnect();

        return $this->id;
    }

    public function insertOrIgnore(): int {
        $this->setCreatedAt();
        $this->setUpdatedAt();

        $data = [];

        foreach ($this->meta->properties as $property) {
            $data[$property->column] = $this->processPropertiesInsert($property);
        }

        $this->query = useDB()->table($this->meta->table);

        $this->id = $this->query->insertOrIgnore($data);

        $this->setSource(DB::class);

        $this->disconnect();

        return $this->id;
    }

    public function update(): void {
        $this->setUpdatedAt();

        $data = [];

        foreach ($this->meta->properties as $property) {
            $data[$property->column] = $this->processPropertiesInsert($property);
        }

        $this->query = useDB()->table($this->meta->table);

        $this->query->where('id', ComparisonType::EQUAL->value, $this->id)
            ->update($data);

        if ($this->redis) {
            useRedis()->update($this->meta->table, $this->id, $data);
        }

        if ($this->meiliSearch) {
            useMeiliSearch()->update($this->meta->table, $this->id, $data);
        }

        $this->setSource(DB::class);

        $this->disconnect();
    }

    public function delete(): void {
        if ($this->deleted) {
            return;
        }

        $this->query = useDB()->table($this->meta->table);

        $this->query->delete($this->id);

        if ($this->redis) {
            useRedis()->delete($this->meta->table, $this->id);
        }

        if ($this->meiliSearch) {
            useMeiliSearch()->delete($this->meta->table, $this->id);
        }

        $this->setSource(DB::class);

        $this->disconnect();

        $this->deleted = true;
    }

    public static function fetch(): static {
        /** @phpstan-ignore-next-line */
        $static = new static();

        $static->query         = useDB()->table($static->meta->table);
        $static->query_replica = useReplica()->table($static->meta->table);

        return $static;
    }

    public static function truncate(): void {
        /** @phpstan-ignore-next-line */
        $static = new static();

        $static->query = useDB()->table($static->meta->table);

        if ($static->redis) {
            useRedis()->truncate($static->meta->table);
        }

        if ($static->meiliSearch) {
            useMeiliSearch()->truncate($static->meta->table);
        }

        if (strtolower(useDB()->getDriverName()) === 'mysql') {
            useDB()->statement('SET FOREIGN_KEY_CHECKS=0');
        }

        $static->query->truncate();

        if (strtolower(useDB()->getDriverName()) === 'mysql') {
            useDB()->statement('SET FOREIGN_KEY_CHECKS=1');
        }

        $static->disconnect();
    }

    public function byId(int $id): ?static {
        $data        = null;
        $redis       = false;
        $meiliSearch = false;

        if ($this->redis) {
            $redis = useRedis()->single($this->meta->table, $id);

            if ($redis) {
                foreach ($this->meta->properties as $property) {
                    if (!isset($redis->{$property->column}) && !$property->type->allowsNull()) {
                        $redis = false;

                        break;
                    }
                }
            }

            if ($redis) {
                $this->setSource(Redis::class);

                $data = $redis;
            }
        }

        if (!$data && $this->meiliSearch) {
            $meiliSearch = useMeiliSearch()->single($this->meta->table, $id);

            if ($meiliSearch) {
                foreach ($this->meta->properties as $property) {
                    if (!isset($meiliSearch->{$property->column}) && !$property->type->allowsNull()) {
                        $meiliSearch = false;

                        break;
                    }
                }
            }

            if ($meiliSearch) {
                $this->setSource(MeiliSearch::class);

                $data = $meiliSearch;
            }
        }

        if (!$data) {
            $data = $this->query_replica->find($id);

            if (!$data) {
                $data = $this->query->find($id);
            }

            if ($data) {
                $this->setSource(DB::class);
            }
        }

        if (!$data) {
            $this->disconnect();

            return null;
        }

        if ($this->redis && !$redis) {
            useRedis()->insert($this->meta->table, $id, (array)$data);
        }

        if ($this->meiliSearch && !$meiliSearch) {
            useMeiliSearch()->insert($this->meta->table, $id, (array)$data);
        }

        $this->disconnect();

        foreach ($this->meta->properties as $property) {
            $this->processPropertiesFetch($this, $property, $data);
        }

        return $this;
    }

    public function single(): ?static {
        $this->query_replica->limit(1);
        $this->query->limit(1);
        $data        = null;
        $redis       = false;
        $meiliSearch = false;

        if ($this->redis) {
            $redis = useRedis()->collection($this->meta->table, $this->query);

            if (count($redis) > 0) {
                $item = $redis[0];

                foreach ($this->meta->properties as $property) {
                    if (!isset($item->{$property->column}) && !$property->type->allowsNull()) {
                        $redis = false;

                        break;
                    }
                }

                if ($redis) {
                    $this->setSource(Redis::class);

                    $data = $item;
                }
            }
        }

        if (!$data) {
            $data = $this->query_replica->get()->first();

            if (!$data) {
                $data = $this->query->get()->first();
            }

            if ($data) {
                $this->setSource(DB::class);
            }
        }

        if (!$data) {
            $this->disconnect();

            return null;
        }

        if ($this->redis && !$redis) {
            useRedis()->set($this->meta->table, $this->query, [(array)$data]);
        }

        $this->disconnect();

        foreach ($this->meta->properties as $property) {
            $this->processPropertiesFetch($this, $property, $data);
        }

        return $this;
    }

    /**
     * @return list<static>
     */
    public function all(): array {
        $data        = null;
        $redis       = false;
        $meiliSearch = false;

        if ($this->redis) {
            $redis = useRedis()->collection($this->meta->table, $this->query);

            if (count($redis) > 0) {
                foreach ($redis as $item) {
                    foreach ($this->meta->properties as $property) {
                        if (!isset($item->{$property->column}) && !$property->type->allowsNull()) {
                            $redis = false;

                            break 2;
                        }
                    }
                }

                if ($redis) {
                    $this->setSource(Redis::class);

                    $data = $redis;
                }
            }
        }

        if (!$data) {
            $data = $this->query_replica->get()->all();

            if (count($data) === 0) {
                $data = $this->query->get()->all();
            }

            if (count($data) > 0) {
                $this->setSource(DB::class);
            }
        }

        if (count($data) === 0) {
            $this->disconnect();

            return [];
        }

        if ($this->redis && !$redis) {
            useRedis()->set($this->meta->table, $this->query, $data);
        }

        $this->disconnect();

        $models = [];

        foreach ($data as $item) {
            /** @phpstan-ignore-next-line */
            $model = new static();

            $model->setSource($this->source);

            foreach ($this->meta->properties as $property) {
                $this->processPropertiesFetch($model, $property, $item);
            }

            $models[] = $model;
        }

        return $models; // @phpstan-ignore-line
    }

    /**
     * @return list<static>
     */
    public function search(string $term): array {
        $data        = [];
        $meiliSearch = false;

        if ($this->meiliSearch) {
            $meiliSearch = useMeiliSearch()->search($this->meta->table, $term);

            if (count($meiliSearch) > 0) {
                foreach ($meiliSearch as $item) {
                    foreach ($this->meta->properties as $property) {
                        if (!isset($item->{$property->column}) && !$property->type->allowsNull()) {
                            $meiliSearch = false;

                            break 2;
                        }
                    }
                }

                if ($meiliSearch) {
                    $this->setSource(MeiliSearch::class);

                    $data = $meiliSearch;
                }
            }
        }

        $this->disconnect();

        if (!$meiliSearch) {
            return [];
        }

        $models = [];

        foreach ($data as $item) {
            /** @phpstan-ignore-next-line */
            $model = new static();
            $model->setSource($this->source);

            foreach ($this->meta->properties as $property) {
                $this->processPropertiesFetch($model, $property, $item);
            }

            $models[] = $model;
        }

        return $models; // @phpstan-ignore-line
    }

    public function by(string $column, null|int|string $value): static {
        $this->where($column, $value);

        return $this;
    }

    public function limit(int $limit): static {
        $this->query_replica->limit($limit);
        $this->query->limit($limit);

        return $this;
    }

    public function offset(int $offset): static {
        $this->query_replica->offset($offset);
        $this->query->offset($offset);

        return $this;
    }

    public function where(
        string $column,
        null|array|int|string $value,
        ComparisonType $comparison = ComparisonType::EQUAL
    ): static {
        if ($comparison === ComparisonType::IN) {
            $this->query_replica->whereIn($column, $value);
            $this->query->whereIn($column, $value);
        } else {
            $this->query_replica->where($column, $comparison->value, $value);
            $this->query->where($column, $comparison->value, $value);
        }

        return $this;
    }

    public function order(string $by, OrderType $type = OrderType::ASC): static {
        $this->query_replica->orderBy($by, $type->value);
        $this->query->orderBy($by, $type->value);

        return $this;
    }

    public function refresh(): ?static {
        return static::fetch()->byId($this->id);
    }

    abstract public function jsonSerialize(): array;

    /**
     * @template T of Model
     *
     * @param class-string<T> $class
     *
     * @return T
     */
    public function belongsTo(string $class): Model {
        $property = strtolower(
            /** @phpstan-ignore-next-line */
            preg_replace('/\B([A-Z])/', '_$1', (new ReflectionClass($class))->getShortName())
        ) . '_id';

        $id = $this->{$property};

        if (!is_numeric($id) || $id === 0) {
            throw new NanoException("invalid id for belongsTo entity '{$class}'", HttpCode::INTERNAL_SERVER_ERROR->code());
        }

        $obj = call_user_func([$class, 'fetch'])->byId((int)$id);

        if (!$obj) {
            throw new NanoException("related belongsTo entity '{$class}' not found", HttpCode::INTERNAL_SERVER_ERROR->code());
        }

        return $obj;
    }

    /**
     * @template T of Model
     *
     * @param class-string<T> $class
     *
     * @return T
     */
    public function hasOne(string $class): Model {
        $property = $this->meta->table . '_id';

        $obj = call_user_func([$class, 'fetch'])->by($property, $this->id)->single();

        if (!$obj) {
            throw new NanoException("related hasOne entity '{$class}' not found", HttpCode::INTERNAL_SERVER_ERROR->code());
        }

        return $obj;
    }

    /**
     * @template T of Model
     *
     * @param class-string<T> $class
     *
     * @return null|T
     */
    public function maybeHasOne(string $class): ?Model {
        $property = $this->meta->table . '_id';

        return call_user_func([$class, 'fetch'])->by($property, $this->id)->single();
    }

    /**
     * @template T of Model
     *
     * @param class-string<T> $class
     *
     * @return T[]
     */
    public function hasMany(string $class): array {
        $property = $this->meta->table . '_id';

        return call_user_func([$class, 'fetch'])->by($property, $this->id)->all();
    }

    public function count(): int {
        $count = $this->query_replica->count();

        if (!$count) {
            $count = $this->query->count();
        }

        $this->disconnect();

        return $count;
    }

    public function meta(): Meta {
        return $this->meta;
    }

    public function is_deleted(): bool {
        return $this->deleted;
    }

    private function setUpdatedAt(): void {
        $this->updated_at = Carbon::now('UTC')->toDateTimeString();
    }

    private function setCreatedAt(): void {
        $this->created_at = Carbon::now('UTC')->toDateTimeString();
    }

    private function setSource(string $connectorClass): void {
        $class = explode('\\', strtolower($connectorClass));

        /** @phpstan-ignore-next-line */
        $this->source = array_pop($class);
    }

    private function disconnect(): void {
        unset($this->query, $this->query_replica);
    }

    private function processPropertiesInsert(Property $property): mixed {
        if (!isset($this->{$property->name})) {
            if ($property->type->allowsNull()) {
                $this->{$property->name} = null;

                return null;
            }

            useNanoException("property '{$property->name}' is not set in model '{$this->meta->class}'");
        } else {
            $prop = $this->{$property->name};

            if ($property->type->isBuiltin()) {
                // simple properties
                if ($property->type->getName() === 'array') {
                    return json_encode($prop);
                }

                return $prop;
            }
            // complex properties
            /** @phpstan-ignore-next-line */
            $reflection = new ReflectionClass($property->type->getName());

            if ($reflection->isEnum()) {
                if (!$prop instanceof BackedEnum) {
                    throw new NanoException("property '{$property->name}' is not a valid enum in model '{$this->meta->class}'");
                }
                
                return $prop->value;
            }

            if ($reflection->isSubclassOf(Model::class)) {
                if (!$prop instanceof Model) {
                    throw new NanoException("property '{$property->name}' is not a valid model im '{$this->meta->class}'");
                }

                if (!isset($prop->id)) {
                    $prop->insert();
                }

                return $prop->id;
            }

            useNanoException(
                "property '{$property->name}' is not a valid type in model '{$this->meta->class}'"
            );
        }

        return null;
    }

    private function processPropertiesFetch(Model &$model, Property $property, object $data): void {
        if (!isset($data->{$property->column})) {
            if (!$property->type->allowsNull()) {
                useNanoException("property '{$property->name}' is not set in model '{$model->meta->class}'");
            }

            $model->{$property->name} = null;
        } else {
            $value = $data->{$property->column};

            if ($property->type->isBuiltin()) {
                // simple properties
                if ($property->type->getName() === 'array' && is_string($value)) {
                    $model->{$property->name} = json_decode($value, true);
                } else {
                    $model->{$property->name} = $value;
                }
            } else {
                // complex properties
                /** @phpstan-ignore-next-line */
                $reflection = new ReflectionClass($property->type->getName());

                if ($reflection->isEnum()) {
                    $model->{$property->name} = call_user_func(
                        /** @phpstan-ignore-next-line */
                        [$property->type->getName(), 'from'],
                        $value
                    );
                } elseif ($reflection->isSubclassOf(Model::class)) {
                    /** @phpstan-ignore-next-line */
                    $model->{$property->name} = call_user_func(
                        /** @phpstan-ignore-next-line */
                        [$property->type->getName(), 'fetch'],
                    )->byId($value);
                } else {
                    useNanoException(
                        "property '{$property->name}' is not a valid type in model '{$model->meta->class}'"
                    );
                }
            }
        }

        // @phpstan-ignore-next-line
        if (isset($data->id) && is_numeric($data->id)) {
            $model->id = (int)$data->id;
        }
    }
}
