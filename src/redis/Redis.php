<?php

namespace laylatichy\nano\modules\orm\redis;

use Illuminate\Database\Query\Builder;
use laylatichy\nano\modules\orm\Connector;
use Predis\Collection\Iterator\Keyspace;
use stdClass;
use Throwable;

class Redis extends Connector {
    public RedisConnection $connection;

    public int $expire;

    public function setConnection(RedisConnection $connection, int $expire): void {
        if (isset($this->connection)) {
            useNanoException('redis connection already set');
        }

        $this->connection = $connection;

        $this->expire = $expire;
    }

    public function insert(string $table, int $id, array $data): void {
        try {
            $this->removeCollections($table);

            $key = "{$table}:{$id}";

            $this->connection->client->set($key, json_encode(['id' => $id, ...$data]));
            $this->connection->client->expire($key, $this->expire);
        } catch (Throwable $e) {
            useNanoException("redis insert failed: {$e->getMessage()}");
        }
    }

    public function update(string $table, int $id, array $data): void {
        try {
            $this->removeCollections($table);

            $key = "{$table}:{$id}";

            $this->connection->client->set($key, json_encode(['id' => $id, ...$data]));
            $this->connection->client->expire($key, $this->expire);
        } catch (Throwable $e) {
            useNanoException("redis update failed: {$e->getMessage()}");
        }
    }

    public function delete(string $table, int $id): void {
        try {
            $this->removeCollections($table);

            $key = "{$table}:{$id}";

            $this->connection->client->del($key);
        } catch (Throwable $e) {
            useNanoException("redis delete failed: {$e->getMessage()}");
        }
    }

    public function single(string $table, int $id): ?stdClass {
        try {
            $key = "{$table}:{$id}";

            $item = $this->connection->client->get($key);
        } catch (Throwable $e) {
            useNanoException("redis single failed: {$e->getMessage()}");
        }

        if (isset($item)) {
            $data = json_decode($item);

            if ($data instanceof stdClass) {
                return $data;
            }
        }

        return null;
    }

    /**
     * @return stdClass[]
     */
    public function collection(string $table, Builder $query): array {
        try {
            $key = $this->generateKey($table, $query);

            $items = $this->connection->client->get($key);

            if ($items) {
                $items = json_decode($items);

                if (is_array($items)) {
                    return $items; // @phpstan-ignore-line
                }
            }
        } catch (Throwable $e) {
            useNanoException("redis collection failed: {$e->getMessage()}");
        }

        return [];
    }

    public function set(string $table, Builder $query, array $items): void {
        try {
            $key = $this->generateKey($table, $query);

            $this->connection->client->set($key, json_encode($items));
            $this->connection->client->expire($key, $this->expire);
        } catch (Throwable $e) {
            useNanoException("redis set failed: {$e->getMessage()}");
        }
    }

    public function removeCollections(string $table): void {
        try {
            foreach (new Keyspace($this->connection->client, "{$table}:collection:*") as $key) {
                if (is_string($key)) {
                    $this->connection->client->del($key);
                }
            }
        } catch (Throwable $e) {
            useNanoException("redis remove collections failed: {$e->getMessage()}");
        }
    }

    public function generateKey(string $table, Builder $query): string {
        $data = [];

        $data['wheres'] = $query->wheres;
        $data['orders'] = $query->orders;
        $data['limit']  = $query->limit;
        $data['offset'] = $query->offset;

        $key = "{$table}:collection:";

        /** @phpstan-ignore-next-line */
        $key .= hash('sha512', json_encode($data));

        return $key;
    }

    public function truncate(string $table): void {
        try {
            $this->removeCollections($table);

            foreach (new Keyspace($this->connection->client, "{$table}:*") as $key) {
                if (is_string($key)) {
                    $this->connection->client->del($key);
                }
            }
        } catch (Throwable $e) {
            useNanoException("redis truncate failed: {$e->getMessage()}");
        }
    }
}
