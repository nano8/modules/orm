<?php

namespace laylatichy\nano\modules\orm\redis;

use Exception;
use laylatichy\nano\modules\orm\Connection;
use Predis\Client;

class RedisConnection extends Connection {
    public Client $client;

    public function __construct(
        private readonly string $host,
        private readonly int $port,
        private readonly string $password,
    ) {
        $this->connect();
    }

    public function connect(): void {
        $this->client = new Client([
            'host'       => $this->host,
            'port'       => $this->port,
            'password'   => $this->password,
            'persistent' => true,
        ]);

        try {
            $this->client->ping();
        } catch (Exception $e) {
            useNanoException("redis connection failed: {$e->getMessage()}");
        }
    }

    public function disconnect(): void {
        $this->client->disconnect();

        unset($this->client);
    }
}
