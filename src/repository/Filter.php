<?php

namespace laylatichy\nano\modules\orm\repository;

use laylatichy\nano\modules\orm\model\enums\ComparisonType;

class Filter {
    public function __construct(
        public readonly string $column,
        public readonly string $value,
        public readonly ComparisonType $comparison,
    ) {}
}