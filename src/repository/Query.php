<?php

namespace laylatichy\nano\modules\orm\repository;

use laylatichy\nano\core\request\Request;
use laylatichy\nano\modules\orm\model\enums\ComparisonType;
use laylatichy\nano\modules\orm\model\enums\OrderType;

class Query {
    public int $limit  = 100;

    public ?string $before = null;

    public ?string $after = null;

    public string $order_by = 'id';

    public OrderType $order_type = OrderType::ASC;

    /**
     * @var Filter[]
     */
    public array $filters = [];

    /**
     * @var string[]
     */
    public array $errors = [];

    public function __construct(Request $request) {
        $limit = $request->get('limit');

        if (!is_null($limit) && !(is_string($limit) || is_numeric($limit))) {
            $this->errors[] = 'invalid limit';
        } elseif (!is_null($limit) && (int)$limit === 0) {
            $this->limit = 0;
        } else {
            $this->limit = $limit ? (int)$limit : 100;
        }

        $before = $request->get('before');

        if (!is_null($before) && !(is_string($before) || is_numeric($before))) {
            $this->errors[] = 'invalid before';
        } else {
            $this->before = $before ? (string)$before : null;
        }

        $after = $request->get('after');

        if (!is_null($after) && !(is_string($after) || is_numeric($after))) {
            $this->errors[] = 'invalid after';
        } else {
            if ($this->before && $after) {
                $this->errors[] = 'before and after cannot be used together';
            } else {
                $this->after = $after ? (string)$after : null;
            }
        }

        $order_by = $request->get('order_by');

        if (!is_null($order_by) && !is_string($order_by)) {
            $this->errors[] = 'invalid order_by';
        } else {
            $this->order_by = $order_by ?? 'id';
        }

        $order_type = $request->get('order_type');

        if (!is_null($order_type) && !is_string($order_type)) {
            $this->errors[] = 'invalid order_type';
        } else {
            if ($order_type) {
                $type = OrderType::tryFrom($order_type);

                $this->order_type = $type ?? OrderType::ASC;
            }
        }

        $filters = is_string($request->get('filters'))
            ? json_decode($request->get('filters'), true)
            : (is_array($request->get('filters'))
                ? $request->get('filters')
                : null);

        if (!is_null($filters) && !is_array($filters)) {
            $this->errors[] = 'invalid filters';
        } elseif (is_array($filters)) {
            $this->filters = $this->generateFilters($filters);
        }

        if (count($this->errors) > 0) {
            useBadRequestException($this->errors);
        }
    }

    /**
     * @return Filter[]
     */
    private function generateFilters(array $filters): array {
        $data = [];

        if (count($filters) === 0) {
            return $data;
        }

        foreach ($filters as $key => $filter) {
            if (!is_array($filter)) {
                $this->errors[] = "invalid filter for {$key}";

                continue;
            }

            if (!isset($filter['value'])) {
                $this->errors[] = "invalid filter value for {$key}";
            } else {
                $value = $filter['value'];
            }

            if (isset($filter['type'])) {
                $type = $filter['type'];

                if (is_numeric($type)) {
                    $type = (string)$type;
                }

                if (!is_string($type)) {
                    $this->errors[] = "invalid filter type for {$key}";
                } else {
                    $comparison = ComparisonType::tryFrom($type)
                        ?? ComparisonType::tryFromCase($type);

                    if (!$comparison) {
                        $this->errors[] = "invalid filter type for {$key}";
                    }
                }
            }

            if (!isset($value)) {
                continue;
            }

            if (is_numeric($value)) {
                $value = (string)$value;
            }

            if (!is_string($value)) {
                continue;
            }

            if (!isset($comparison)) {
                $comparison = ComparisonType::EQUAL;
            }

            $data[] = new Filter($key, $value, $comparison);
        }

        return $data;
    }
}
