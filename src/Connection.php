<?php

namespace laylatichy\nano\modules\orm;

abstract class Connection {
    abstract public function connect(): void;

    abstract public function disconnect(): void;
}
