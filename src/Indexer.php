<?php

namespace laylatichy\nano\modules\orm;

use stdClass;

interface Indexer {
    /**
     * @return stdClass[]
     */
    public function search(string $table, string $term): array;
}
