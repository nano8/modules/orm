<?php

namespace laylatichy\nano\modules\orm\meilisearch;

use Exception;
use laylatichy\nano\core\exception\NanoException;
use laylatichy\nano\modules\orm\Connection;
use Meilisearch\Client;

class MeiliSearchConnection extends Connection {
    public Client $client;

    public function __construct(
        private readonly string $host,
        private readonly int $port,
        private readonly string $password,
    ) {
        $this->connect();
    }

    public function connect(): void {
        try {
            $this->client = new Client("{$this->host}:{$this->port}", $this->password);

            if (!$this->client->isHealthy()) {
                $message = '';

                if (is_array($this->client->health())) {
                    foreach ($this->client->health() as $key => $value) {
                        if (!is_string($value)) {
                            continue;
                        }

                        $message .= "{$key}: {$value}\n";
                    }
                }

                useNanoException("meilisearch connection failed: {$message}");
            }
        } catch (NanoException $e) {
            useNanoException($e->getMessage());
        } catch (Exception $e) {
            useNanoException("meilisearch connection failed: {$e->getMessage()}");
        }
    }

    public function disconnect(): void {
        unset($this->client);
    }
}
