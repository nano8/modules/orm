<?php

namespace laylatichy\nano\modules\orm\meilisearch;

use Exception;
use Illuminate\Database\Query\Builder;
use laylatichy\nano\modules\orm\Connector;
use laylatichy\nano\modules\orm\Indexer;
use Meilisearch\Endpoints\Indexes;
use stdClass;
use Throwable;

class MeiliSearch extends Connector implements Indexer {
    public MeiliSearchConnection $connection;

    public function setConnection(MeiliSearchConnection $connection): void {
        if (isset($this->connection)) {
            useNanoException('meilisearch connection already set');
        }

        $this->connection = $connection;
    }

    public function insert(string $table, int $id, array $data): void {
        $index = $this->getIndex($table);

        $index->addDocuments(['id' => $id, ...$data], 'id');
    }

    public function update(string $table, int $id, array $data): void {
        $index = $this->getIndex($table);

        $index->updateDocuments(['id' => $id, ...$data], 'id');
    }

    public function delete(string $table, int $id): void {
        $index = $this->getIndex($table);

        $index->deleteDocument($id);
    }

    public function single(string $table, int $id): ?stdClass {
        $index = $this->getIndex($table);

        try {
            $data = $index->getDocument($id);
        } catch (Exception) {
            return null;
        }

        return (object)$data;
    }

    public function search(string $table, string $term): array {
        $data = [];

        try {
            $index = $this->getIndex($table);

            $data = $index->search($term, [
                'limit' => 100,
            ])->getHits();
        } catch (Throwable $e) {
            useNanoException("meilisearch search failed: {$e->getMessage()}");
        }

        return array_map(function (array $item) {
            return (object)$item;
        }, $data);
    }

    public function collection(string $table, Builder $query): array {
        // TODO: Implement collection() method.

        return [];
    }

    public function set(string $table, Builder $query, array $items): void {
        // TODO: Implement set() method.
    }
    
    public function truncate(string $table): void {
        $index = $this->getIndex($table);

        $index->deleteAllDocuments();
    }

    private function getIndex(string $table): Indexes {
        try {
            $index = $this->connection->client->getIndex($table);

            $attributes = $index->getFilterableAttributes();

            if (!in_array('id', $attributes, true)) {
                $attributes[] = 'id';

                $index->updateFilterableAttributes($attributes);
            }
        } catch (Exception) {
            $this->connection->client->createIndex($table);

            $index = $this->connection->client->index($table);
        }

        return $index;
    }
}
