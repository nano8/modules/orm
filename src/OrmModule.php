<?php

namespace laylatichy\nano\modules\orm;

use laylatichy\nano\modules\NanoModule;

use laylatichy\nano\Nano;

class OrmModule implements NanoModule {
    public Orm $orm;

    public function __construct() {}

    public function register(Nano $nano): void {
        if (isset($this->orm)) {
            useNanoException('orm module already registered');
        }

        $this->orm = new Orm();

        $this->load();
    }

    private function load(): void {
        $file = useConfig()->getRoot() . '/../.config/db.php';

        if (file_exists($file)) {
            require $file;
        }
    }
}
