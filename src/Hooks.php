<?php

use Illuminate\Database\Connection;
use laylatichy\nano\core\request\Request;
use laylatichy\nano\modules\orm\db\DBConnection;
use laylatichy\nano\modules\orm\meilisearch\MeiliSearch;
use laylatichy\nano\modules\orm\meilisearch\MeiliSearchConnection;
use laylatichy\nano\modules\orm\Model;
use laylatichy\nano\modules\orm\OrmModule;
use laylatichy\nano\modules\orm\redis\Redis;
use laylatichy\nano\modules\orm\redis\RedisConnection;
use laylatichy\nano\modules\orm\Repository;

if (!function_exists('defineRedis')) {
    function defineRedis(string $host, int $port, string $password, int $expire = 3600): void {
        useNanoModule(OrmModule::class)
            ->orm
            ->redis
            ->setConnection(
                new RedisConnection(
                    $host,
                    $port,
                    $password
                ),
                $expire,
            );
    }
}

if (!function_exists('useRedis')) {
    function useRedis(): Redis {
        return useNanoModule(OrmModule::class)
            ->orm
            ->redis;
    }
}

if (!function_exists('defineDB')) {
    function defineDB(
        string $host,
        int $port,
        string $database,
        string $user,
        string $password,
        string $driver = 'mysql',
        string $charset = 'utf8mb4',
        string $collation = 'utf8mb4_unicode_ci',
        array $options = [
            PDO::ATTR_PERSISTENT => true,
        ],
    ): void {
        useNanoModule(OrmModule::class)
            ->orm
            ->db
            ->setConnection(
                new DBConnection(
                    $host,
                    $port,
                    $database,
                    $user,
                    $password,
                    $driver,
                    $charset,
                    $collation,
                    $options,
                ),
            );
    }
}

if (!function_exists('defineReplica')) {
    function defineReplica(
        string $host,
        int $port,
        string $database,
        string $user,
        string $password,
        string $driver = 'mysql',
        string $charset = 'utf8mb4',
        string $collation = 'utf8mb4_unicode_ci',
        array $options = [
            PDO::ATTR_PERSISTENT => true,
        ],
    ): void {
        useNanoModule(OrmModule::class)
            ->orm
            ->replica
            ->setConnection(
                new DBConnection(
                    $host,
                    $port,
                    $database,
                    $user,
                    $password,
                    $driver,
                    $charset,
                    $collation,
                    $options,
                ),
            );
    }
}

if (!function_exists('useDB')) {
    function useDB(): Connection {
        return useNanoModule(OrmModule::class)
            ->orm
            ->db
            ->connection
            ->client
            ->getConnection('default');
    }
}

if (!function_exists('useReplica')) {
    function useReplica(): Connection {
        $target = useNanoModule(OrmModule::class)
            ->orm
            ->replica;

        if (!isset($target->connection)) {
            $target = useNanoModule(OrmModule::class)
                ->orm
                ->db;
        }

        return $target
            ->connection
            ->client
            ->getConnection('default');
    }
}

if (!function_exists('defineMeiliSearch')) {
    function defineMeiliSearch(string $host, int $port, string $password): void {
        useNanoModule(OrmModule::class)
            ->orm
            ->meilisearch
            ->setConnection(
                new MeiliSearchConnection(
                    $host,
                    $port,
                    $password
                ),
            );
    }
}

if (!function_exists('useMeiliSearch')) {
    function useMeiliSearch(): MeiliSearch {
        return useNanoModule(OrmModule::class)
            ->orm
            ->meilisearch;
    }
}

if (!function_exists('useCollection')) {
    function useCollection(Model $model, Request $request): array {
        return (new Repository($model, $request))->fetch();
    }
}

if (!function_exists('useRepository')) {
    /**
     * @template T of Model
     *
     * @param class-string<T> $class
     */
    function useRepository(string $class, Request $request): array {
        return useCollection($class::fetch(), $request);
    }
}