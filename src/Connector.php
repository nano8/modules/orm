<?php

namespace laylatichy\nano\modules\orm;

use Illuminate\Database\Query\Builder;
use stdClass;

abstract class Connector {
    abstract public function insert(string $table, int $id, array $data): void;

    abstract public function update(string $table, int $id, array $data): void;

    abstract public function delete(string $table, int $id): void;

    abstract public function single(string $table, int $id): ?stdClass;

    /**
     * @return stdClass[]
     */
    abstract public function collection(string $table, Builder $query): array;

    abstract public function set(string $table, Builder $query, array $items): void;
}
