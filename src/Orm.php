<?php

namespace laylatichy\nano\modules\orm;

use laylatichy\nano\modules\orm\db\DB;
use laylatichy\nano\modules\orm\meilisearch\MeiliSearch;
use laylatichy\nano\modules\orm\redis\Redis;

class Orm {
    public function __construct(
        public readonly DB $db = new DB(),
        public readonly Redis $redis = new Redis(),
        public readonly MeiliSearch $meilisearch = new MeiliSearch(),
        public readonly DB $replica = new DB(),
    ) {}
}
