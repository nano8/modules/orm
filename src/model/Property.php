<?php

namespace laylatichy\nano\modules\orm\model;

use laylatichy\nano\modules\orm\Model;
use ReflectionClass;
use ReflectionNamedType;
use ReflectionProperty;

class Property {
    public readonly string $name;

    public string $column;

    public readonly ReflectionNamedType $type;

    public readonly ReflectionProperty $reflection;

    public function __construct(ReflectionProperty $property) {
        $this->reflection = $property;

        $this->name = $property->getName();

        $this->column = strtolower(
            /** @phpstan-ignore-next-line */
            preg_replace('/\B([A-Z])/', '_$1', $this->name)
        );

        if (!$property->getType()) {
            useNanoException("property {$this->name} has no type");
        }

        /** @phpstan-ignore-next-line */
        $this->type = $property->getType();

        /** @phpstan-ignore-next-line */
        if (!$this->type->isBuiltin()) {
            /** @phpstan-ignore-next-line */
            $reflection = new ReflectionClass($property->getType()->getName());

            if ($reflection->isSubclassOf(Model::class)) {
                $this->column .= '_id';
            }
        }
    }
}