<?php

namespace laylatichy\nano\modules\orm\model\enums;

enum OrderType: string {
    case ASC  = 'ASC';
    case DESC = 'DESC';
}

