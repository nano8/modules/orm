<?php

namespace laylatichy\nano\modules\orm\model\enums;

enum ComparisonType: string {
    case GREATER_THAN          = '>';
    case GREATER_THAN_OR_EQUAL = '>=';
    case LESS_THAN             = '<';
    case LESS_THAN_OR_EQUAL    = '<=';
    case NOT_EQUAL             = '!=';
    case NULL_SAFE_EQUAL       = '<=>';
    case EQUAL                 = '=';
    case IS                    = 'IS';
    case IS_NOT                = 'IS NOT';
    case IS_NOT_NULL           = 'IS NOT NULL';
    case IS_NULL               = 'IS NULL';
    case LIKE                  = 'LIKE';
    case NOT_LIKE              = 'NOT LIKE';
    case IN                    = 'IN';

    public static function tryFromCase(string $case): ?self {
        return match ($case) {
            'GREATER_THAN'          => self::GREATER_THAN,
            'GREATER_THAN_OR_EQUAL' => self::GREATER_THAN_OR_EQUAL,
            'LESS_THAN'             => self::LESS_THAN,
            'LESS_THAN_OR_EQUAL'    => self::LESS_THAN_OR_EQUAL,
            'NOT_EQUAL'             => self::NOT_EQUAL,
            'NULL_SAFE_EQUAL'       => self::NULL_SAFE_EQUAL,
            'EQUAL'                 => self::EQUAL,
            'IS'                    => self::IS,
            'IS_NOT'                => self::IS_NOT,
            'IS_NOT_NULL'           => self::IS_NOT_NULL,
            'IS_NULL'               => self::IS_NULL,
            'LIKE'                  => self::LIKE,
            'NOT_LIKE'              => self::NOT_LIKE,
            'IN'                    => self::IN,
            default                 => null,
        };
    }
}

