<?php

namespace laylatichy\nano\modules\orm\model;

use laylatichy\nano\modules\orm\Model;
use ReflectionClass;
use ReflectionProperty;

class Meta {
    private const IGNORED_PROPERTIES = [
        'id',
        'source',
        'redis',
        'meiliSearch',
    ];

    public readonly string $class;

    public readonly string $table;

    /**
     * @var Property[]
     */
    public array $properties = [];

    /**
     * @var ReflectionClass<Model>
     */
    public readonly ReflectionClass $reflection;

    /**
     * @param class-string<Model> $class
     */
    public function __construct(string $class) {
        $this->class = $class;

        $this->reflection = new ReflectionClass($class);

        $this->table = strtolower(
            /** @phpstan-ignore-next-line */
            preg_replace('/\B([A-Z])/', '_$1', $this->reflection->getShortName())
        );

        $properties = array_filter(
            $this->reflection->getProperties(),
            fn (ReflectionProperty $p) => !in_array(
                $p->getName(),
                self::IGNORED_PROPERTIES,
                true,
            )
        );

        foreach ($properties as $property) {
            $prop = new Property($property);

            $this->properties[$prop->column] = $prop;
        }
    }

    public function property(string $key): Property {
        if (!isset($this->properties[$key])) {
            useNanoException("property '{$key}' does not exist in model '{$this->class}'");
        }

        return $this->properties[$key];
    }
}